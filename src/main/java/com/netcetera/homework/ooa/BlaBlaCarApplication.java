package com.netcetera.homework.ooa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlaBlaCarApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlaBlaCarApplication.class, args);
	}
}