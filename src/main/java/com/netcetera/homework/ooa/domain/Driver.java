package com.netcetera.homework.ooa.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;

@Entity
@Table(name = "drivers")
public class Driver extends User {

    private Double rating;

    @Column(name = "driving_licence")
    private String drivingLicence;

    @OneToOne(mappedBy = "driver",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private DriverPreferences driverPreferences;

    public Driver() {
        super();
    }

    public Driver(
            String firstName,
            String lastName,
            String description,
            String username,
            String password,
            ContactInfo contactInfo,
            Double rating,
            String drivingLicence,
            DriverPreferences driverPreferences) {

        super(firstName, lastName, description, username, password, contactInfo);
        this.rating = rating;
        this.drivingLicence = drivingLicence;
        this.driverPreferences = driverPreferences;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getDrivingLicence() {
        return drivingLicence;
    }

    public void setDrivingLicence(String drivingLicence) {
        this.drivingLicence = drivingLicence;
    }

    public DriverPreferences getDriverPreferences() {
        return driverPreferences;
    }

    public void setDriverPreferences(DriverPreferences driverPreferences) {
        this.driverPreferences = driverPreferences;
    }
}
