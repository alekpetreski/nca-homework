package com.netcetera.homework.ooa.domain;

import com.netcetera.homework.ooa.domain.enums.PermissionType;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;

@Entity
@Table(name = "driver_preferences")
public class DriverPreferences extends Preferences {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "driver_id")
    private Driver driver;

    public DriverPreferences() {
        super();
    }

    public DriverPreferences(
            PermissionType pets,
            PermissionType music,
            PermissionType smoking,
            Driver driver) {

        super(pets, music, smoking);
        this.driver = driver;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }
}
