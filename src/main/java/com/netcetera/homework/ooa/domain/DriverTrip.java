package com.netcetera.homework.ooa.domain;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import java.time.LocalDateTime;

@Entity
@Table(name = "driver_trips")
public class DriverTrip extends Trip {

    @Column(name = "total_price")
    private Double totalPrice;

    private LocalDateTime departure;

    private LocalDateTime arrival;

    @ManyToOne
    @JoinColumn(name = "driver_id")
    private Driver driver;

    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    public DriverTrip() {
        super();
    }

    public DriverTrip(
            String origin,
            String destination,
            Integer totalTime,
            Double totalDistance,
            Double totalPrice,
            LocalDateTime departure,
            LocalDateTime arrival,
            Driver driver,
            Vehicle vehicle) {

        super(origin, destination, totalTime, totalDistance);
        this.totalPrice = totalPrice;
        this.departure = departure;
        this.arrival = arrival;
        this.driver = driver;
        this.vehicle = vehicle;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDateTime getDeparture() {
        return departure;
    }

    public void setDeparture(LocalDateTime departure) {
        this.departure = departure;
    }

    public LocalDateTime getArrival() {
        return arrival;
    }

    public void setArrival(LocalDateTime arrival) {
        this.arrival = arrival;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
}
