package com.netcetera.homework.ooa.domain;

import javax.persistence.*;

@Entity
@Table(name = "passengers")
public class Passenger extends User {

    @OneToOne(mappedBy = "passenger",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private PassengerPreferences passengerPreferences;

    public Passenger() {
        super();
    }

    public Passenger(
            String firstName,
            String lastName,
            String description,
            String username,
            String password,
            ContactInfo contactInfo,
            PassengerPreferences passengerPreferences) {

        super(firstName, lastName, description, username, password, contactInfo);
        this.passengerPreferences = passengerPreferences;
    }

    public PassengerPreferences getPassengerPreferences() {
        return passengerPreferences;
    }

    public void setPassengerPreferences(PassengerPreferences passengerPreferences) {
        this.passengerPreferences = passengerPreferences;
    }
}
