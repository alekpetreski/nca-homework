package com.netcetera.homework.ooa.domain;

import javax.persistence.*;

@Entity
@Table(name = "passenger_flexibilities")
public class PassengerFlexibility extends BaseEntity {

    @Column(name = "time_deviation")
    private Integer timeDeviation;

    @Column(name = "price_deviation")
    private Integer priceDeviation;

    @Column(name = "distance_deviation")
    private Integer distanceDeviation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "passenger_id")
    private Passenger passenger;

    public PassengerFlexibility() {}

    public PassengerFlexibility(
            Passenger passenger,
            Integer timeDeviation,
            Integer priceDeviation,
            Integer distanceDeviation) {

        this.passenger = passenger;
        this.timeDeviation = timeDeviation;
        this.priceDeviation = priceDeviation;
        this.distanceDeviation = distanceDeviation;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Integer getTimeDeviation() {
        return timeDeviation;
    }

    public void setTimeDeviation(Integer timeDeviation) {
        this.timeDeviation = timeDeviation;
    }

    public Integer getPriceDeviation() {
        return priceDeviation;
    }

    public void setPriceDeviation(Integer priceDeviation) {
        this.priceDeviation = priceDeviation;
    }

    public Integer getDistanceDeviation() {
        return distanceDeviation;
    }

    public void setDistanceDeviation(Integer distanceDeviation) {
        this.distanceDeviation = distanceDeviation;
    }
}
