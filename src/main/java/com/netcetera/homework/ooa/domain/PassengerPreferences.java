package com.netcetera.homework.ooa.domain;

import com.netcetera.homework.ooa.domain.enums.PermissionType;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;

@Entity
@Table(name = "passenger_preferences")
public class PassengerPreferences extends Preferences {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "passenger_id")
    private Passenger passenger;

    public PassengerPreferences() {
        super();
    }

    public PassengerPreferences(
            PermissionType pets,
            PermissionType music,
            PermissionType smoking,
            Passenger passenger) {

        super(pets, music, smoking);
        this.passenger = passenger;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }
}