package com.netcetera.homework.ooa.domain;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "passenger_trips")
public class PassengerTrip extends Trip {

    private Double price;

    @ManyToOne
    @JoinColumn(name = "driver_id")
    private Driver driver;

    @ManyToOne
    @JoinColumn(name = "passenger_id")
    private Passenger passenger;

    public PassengerTrip() {
        super();
    }

    public PassengerTrip(
            String origin,
            String destination,
            Integer totalTime,
            Double totalDistance,
            Double price,
            Driver driver,
            Passenger passenger) {

        super(origin, destination, totalTime, totalDistance);
        this.price = price;
        this.driver = driver;
        this.passenger = passenger;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }
}
