package com.netcetera.homework.ooa.domain;

import com.netcetera.homework.ooa.domain.enums.PermissionType;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

@MappedSuperclass
public abstract class Preferences {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    private PermissionType pets;

    @Enumerated(value = EnumType.STRING)
    private PermissionType music;

    @Enumerated(value = EnumType.STRING)
    private PermissionType smoking;

    public Preferences() {}

    public Preferences(
            PermissionType pets,
            PermissionType music,
            PermissionType smoking) {

        this.pets = pets;
        this.music = music;
        this.smoking = smoking;
    }

    public Long getId() {
        return id;
    }

    public PermissionType getPets() {
        return pets;
    }

    public void setPets(PermissionType pets) {
        this.pets = pets;
    }

    public PermissionType getMusic() {
        return music;
    }

    public void setMusic(PermissionType music) {
        this.music = music;
    }

    public PermissionType getSmoking() {
        return smoking;
    }

    public void setSmoking(PermissionType smoking) {
        this.smoking = smoking;
    }
}
