package com.netcetera.homework.ooa.domain;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "subroutes")
public class Subroute extends Trip {

    private Double price;

    @Column(name = "free_seats")
    private Integer freeSeats;

    @ManyToOne
    @JoinColumn(name = "driver_trip_id")
    private DriverTrip driverTrip;

    public Subroute(
            String origin,
            String destination,
            Integer totalTime,
            Double totalDistance,
            Double price,
            Integer freeSeats,
            DriverTrip driverTrip) {

        super(origin, destination, totalTime, totalDistance);
        this.price = price;
        this.freeSeats = freeSeats;
        this.driverTrip = driverTrip;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getFreeSeats() {
        return freeSeats;
    }

    public void setFreeSeats(Integer freeSeats) {
        this.freeSeats = freeSeats;
    }

    public DriverTrip getDriverTrip() {
        return driverTrip;
    }

    public void setDriverTrip(DriverTrip driverTrip) {
        this.driverTrip = driverTrip;
    }
}
