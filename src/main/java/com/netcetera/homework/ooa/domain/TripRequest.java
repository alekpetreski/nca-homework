package com.netcetera.homework.ooa.domain;

import com.netcetera.homework.ooa.domain.enums.TicketType;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

@Entity
@Table(name = "trip_requests")
public class TripRequest extends Trip {

    @Column(name = "price_range_from")
    private Double priceRangeFrom;

    @Column(name = "price_range_to")
    private Double priceRangeTo;

    @Column(name = "requested_seats")
    private Integer requestedSeats;

    @Enumerated(EnumType.STRING)
    private TicketType ticketType;

    @ManyToOne
    @JoinColumn(name = "passenger_id")
    private Passenger passenger;

    public TripRequest() {
        super();
    }

    public TripRequest(
            String origin,
            String destination,
            Integer totalTime,
            Double totalDistance,
            Double priceRangeFrom,
            Double priceRangeTo,
            Integer requestedSeats,
            TicketType ticketType,
            Passenger passenger) {

        super(origin, destination, totalTime, totalDistance);
        this.priceRangeFrom = priceRangeFrom;
        this.priceRangeTo = priceRangeTo;
        this.requestedSeats = requestedSeats;
        this.ticketType = ticketType;
        this.passenger = passenger;
    }

    public Double getPriceRangeFrom() {
        return priceRangeFrom;
    }

    public void setPriceRangeFrom(Double priceRangeFrom) {
        this.priceRangeFrom = priceRangeFrom;
    }

    public Double getPriceRangeTo() {
        return priceRangeTo;
    }

    public void setPriceRangeTo(Double priceRangeTo) {
        this.priceRangeTo = priceRangeTo;
    }

    public Integer getRequestedSeats() {
        return requestedSeats;
    }

    public void setRequestedSeats(Integer requestedSeats) {
        this.requestedSeats = requestedSeats;
    }

    public TicketType getTicketType() {
        return ticketType;
    }

    public void setTicketType(TicketType ticketType) {
        this.ticketType = ticketType;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }
}
