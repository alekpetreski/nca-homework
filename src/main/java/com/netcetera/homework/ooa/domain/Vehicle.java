package com.netcetera.homework.ooa.domain;

import com.netcetera.homework.ooa.domain.enums.VehicleType;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

@Entity
@Table(name = "vehicles")
public class Vehicle extends BaseEntity {

    @Column(name = "car_model")
    private String carModel;

    private String manufacturer;

    @Column(name = "manufacture_year")
    private String manufactureYear;

    @Enumerated(EnumType.STRING)
    @Column(name = "vehicle_type")
    private VehicleType vehicleType;

    @Column(name = "number_of_seats")
    private Integer numberOfSeats;

    @Column(name = "gas_consumption")
    private String gasConsumption;

    @ManyToOne
    @JoinColumn(name = "driver_id")
    private Driver driver;

    public Vehicle() {}

    public Vehicle(
            String carModel,
            String manufacturer,
            String manufactureYear,
            VehicleType vehicleType,
            Integer numberOfSeats,
            String gasConsumption,
            Driver driver) {

        this.carModel = carModel;
        this.manufacturer = manufacturer;
        this.manufactureYear = manufactureYear;
        this.vehicleType = vehicleType;
        this.numberOfSeats = numberOfSeats;
        this.gasConsumption = gasConsumption;
        this.driver = driver;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufactureYear() {
        return manufactureYear;
    }

    public void setManufactureYear(String manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Integer getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public String getGasConsumption() {
        return gasConsumption;
    }

    public void setGasConsumption(String gasConsumption) {
        this.gasConsumption = gasConsumption;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }
}
