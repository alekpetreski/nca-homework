package com.netcetera.homework.ooa.domain.enums;

public enum PermissionType {
    ALLOW,
    DISALLOW
}
