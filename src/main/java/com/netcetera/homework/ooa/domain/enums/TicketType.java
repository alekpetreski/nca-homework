package com.netcetera.homework.ooa.domain.enums;

public enum TicketType {
    ONEWAY,
    ROUND
}
