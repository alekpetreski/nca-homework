package com.netcetera.homework.ooa.domain.enums;

public enum VehicleType {
    HATCHBACK,
    SEDAN,
    MPV,
    SUV,
    CROSSOVER,
    COUPE,
    CABRIOLET,
    VAN
}
