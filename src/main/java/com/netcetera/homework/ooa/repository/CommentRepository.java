package com.netcetera.homework.ooa.repository;

import org.springframework.stereotype.Repository;
import com.netcetera.homework.ooa.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
