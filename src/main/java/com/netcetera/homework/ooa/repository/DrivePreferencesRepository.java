package com.netcetera.homework.ooa.repository;

import org.springframework.stereotype.Repository;
import com.netcetera.homework.ooa.domain.DriverPreferences;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface DrivePreferencesRepository extends JpaRepository<DriverPreferences, Long> {
}
