package com.netcetera.homework.ooa.repository;

import com.netcetera.homework.ooa.domain.Driver;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface DriverRepository extends JpaRepository<Driver, Long> {
}
