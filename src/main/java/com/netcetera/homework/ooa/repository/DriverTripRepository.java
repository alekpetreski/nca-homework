package com.netcetera.homework.ooa.repository;

import org.springframework.stereotype.Repository;
import com.netcetera.homework.ooa.domain.DriverTrip;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface DriverTripRepository extends JpaRepository<DriverTrip, Long> {
}
