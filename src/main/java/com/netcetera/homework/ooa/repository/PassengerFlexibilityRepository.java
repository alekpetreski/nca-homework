package com.netcetera.homework.ooa.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.netcetera.homework.ooa.domain.PassengerFlexibility;

import java.util.Optional;

@Repository
public interface PassengerFlexibilityRepository extends JpaRepository<PassengerFlexibility, Long> {

    Optional<PassengerFlexibility> findByPassengerId(Long passengerId);
}
