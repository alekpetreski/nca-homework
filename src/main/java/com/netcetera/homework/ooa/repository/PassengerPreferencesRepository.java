package com.netcetera.homework.ooa.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.netcetera.homework.ooa.domain.PassengerPreferences;

@Repository
public interface PassengerPreferencesRepository extends JpaRepository<PassengerPreferences, Long> {
}
