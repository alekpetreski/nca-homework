package com.netcetera.homework.ooa.repository;

import org.springframework.stereotype.Repository;
import com.netcetera.homework.ooa.domain.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger, Long> {
}
