package com.netcetera.homework.ooa.repository;

import org.springframework.stereotype.Repository;
import com.netcetera.homework.ooa.domain.PassengerTrip;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PassengerTripRepository extends JpaRepository<PassengerTrip, Long> {
}
