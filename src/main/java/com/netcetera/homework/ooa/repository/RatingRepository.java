package com.netcetera.homework.ooa.repository;

import com.netcetera.homework.ooa.domain.Rating;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {
}
