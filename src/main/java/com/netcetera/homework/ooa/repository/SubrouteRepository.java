package com.netcetera.homework.ooa.repository;

import org.springframework.stereotype.Repository;
import com.netcetera.homework.ooa.domain.Subroute;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface SubrouteRepository extends JpaRepository<Subroute, Long> {

    List<Subroute> findByDriverTripId(Long driverTripId);
}
