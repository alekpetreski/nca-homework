package com.netcetera.homework.ooa.repository;

import org.springframework.stereotype.Repository;
import com.netcetera.homework.ooa.domain.TripRequest;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface TripRequestRepository extends JpaRepository<TripRequest, Long> {
}
