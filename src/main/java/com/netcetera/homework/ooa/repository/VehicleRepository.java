package com.netcetera.homework.ooa.repository;

import org.springframework.stereotype.Repository;
import com.netcetera.homework.ooa.domain.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
}
