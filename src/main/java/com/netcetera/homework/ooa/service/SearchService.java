package com.netcetera.homework.ooa.service;

import com.netcetera.homework.ooa.domain.DriverTrip;
import com.netcetera.homework.ooa.domain.TripRequest;

import java.util.List;

public interface SearchService {

    List<DriverTrip> searchTrips(TripRequest tripRequest);
}
