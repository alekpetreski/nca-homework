package com.netcetera.homework.ooa.service.impl;

import com.netcetera.homework.ooa.domain.*;
import com.netcetera.homework.ooa.exception.EntityNotFoundException;
import com.netcetera.homework.ooa.repository.PassengerFlexibilityRepository;
import org.springframework.stereotype.Service;
import com.netcetera.homework.ooa.service.SearchService;
import com.netcetera.homework.ooa.domain.enums.PermissionType;
import com.netcetera.homework.ooa.repository.SubrouteRepository;
import com.netcetera.homework.ooa.repository.DriverTripRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    private final SubrouteRepository subrouteRepository;
    private final DriverTripRepository driverTripRepository;
    private final PassengerFlexibilityRepository passengerFlexibilityRepository;

    public SearchServiceImpl(
            SubrouteRepository subrouteRepository,
            DriverTripRepository driverTripRepository,
            PassengerFlexibilityRepository passengerFlexibilityRepository) {

        this.subrouteRepository = subrouteRepository;
        this.driverTripRepository = driverTripRepository;
        this.passengerFlexibilityRepository = passengerFlexibilityRepository;
    }

    @Override
    public List<DriverTrip> searchTrips(TripRequest tripRequest) {

        List<DriverTrip> resultTrips = new ArrayList<>();
        List<DriverTrip> driverTrips = driverTripRepository.findAll();
        for(DriverTrip driverTrip : driverTrips) {

            /* If the number of requested seats is larger than
               the actual number of seats in the car, then skip this trip */
            Vehicle vehicle = driverTrip.getVehicle();
            if(vehicle.getNumberOfSeats() < tripRequest.getRequestedSeats()) {
                continue;
            }

            Passenger passenger = tripRequest.getPassenger();
            PassengerFlexibility passengerFlexibility =
                    passengerFlexibilityRepository.findByPassengerId(passenger.getId())
                            .orElseThrow(() ->
                                new EntityNotFoundException("Passenger Flexibility for Passenger with ID " + passenger.getId() + " Not Found")
                            );

            /* If the requested route is not a route (subroute)
               of the driving trip, then skip this trip */
            List<Subroute> requestedRoute =
                    checkSubroutes(driverTrip.getId(), tripRequest.getOrigin(), tripRequest.getDestination(), passengerFlexibility);
            if(requestedRoute == null) {
                continue;
            }

            /* If the number of requested seats is larger than
               the available seats for that route, then skip this trip */
            boolean seatsMatch = checkSeats(requestedRoute, tripRequest.getRequestedSeats());
            if(!seatsMatch) {
                continue;
            }

            /* If the actual price (flexibility included) is not
               in the requested price range, then skip this trip */
            boolean priceMatch =
                    checkPrice(requestedRoute, tripRequest.getPriceRangeFrom(), tripRequest.getPriceRangeTo(), passengerFlexibility);
            if(!priceMatch) {
                continue;
            }

            /* If the requested time in minutes is smaller or bigger than
               the estimated time of the driver (flexibility included), then skip this trip */
            boolean timeMatch = checkTime(requestedRoute, tripRequest.getTotalTime(), passengerFlexibility);
            if(!timeMatch) {
                continue;
            }

            /* If the preferences do not match,
               then skip this trip */
            Driver driver = driverTrip.getDriver();
            boolean preferencesMatch =
                    checkPreferences(driver.getDriverPreferences(), passenger.getPassengerPreferences());
            if(!preferencesMatch) {
                continue;
            }
            resultTrips.add(driverTrip);
        }
        // TODO: Implement the Comparable interface in DriverTrip and Driver in order to sort the list according to driver's rating
        return resultTrips;
    }

    private List<Subroute> checkSubroutes(Long driverTripId, String origin, String destination, PassengerFlexibility passengerFlexibility) {
        int indexOfOrigin = -1;
        int indexOfDestination = -1;

        List<Subroute> subroutes = subrouteRepository.findByDriverTripId(driverTripId);
        for(int i = 0; i < subroutes.size(); i++) {
            if(origin.equals(subroutes.get(i).getOrigin())) {
                indexOfOrigin = i;
            }
            if(indexOfOrigin != -1 && destination.equals(subroutes.get(i).getDestination())) {
                indexOfDestination = i;
                if(indexOfOrigin <= indexOfDestination) {
                    return subroutes.subList(indexOfOrigin, indexOfDestination+1);
                }
            }
        }
        // TODO: Implement the distance flexibility
        return null;
    }

    private boolean checkSeats(List<Subroute> requestedRoute, Integer requestedSeats) {
        for(Subroute subroute : requestedRoute) {
            if(subroute.getFreeSeats() < requestedSeats) {
                return false;
            }
        }
        return true;
    }

    private boolean checkPrice(List<Subroute> requestedRoute, Double priceRangeFrom, Double priceRangeTo, PassengerFlexibility passengerFlexibility) {
        double totalPrice = 0D;
        for(Subroute subroute : requestedRoute) {
            totalPrice += subroute.getPrice();
        }
        return priceRangeFrom <= totalPrice && totalPrice <= priceRangeTo + passengerFlexibility.getPriceDeviation();
    }

    private boolean checkTime(List<Subroute> requestedRoute, Integer requestedTime, PassengerFlexibility passengerFlexibility) {
        int totalTime = 0;
        for(Subroute subroute : requestedRoute) {
            totalTime += subroute.getTotalTime();
        }
        return totalTime < requestedTime + passengerFlexibility.getTimeDeviation();
    }

    private boolean checkPreferences(DriverPreferences driverPreferences, PassengerPreferences passengerPreferences) {
        // TODO: Refactor the code
        if(driverPreferences.getPets() == PermissionType.ALLOW && passengerPreferences.getPets() == PermissionType.ALLOW) {
            if(driverPreferences.getMusic() == PermissionType.ALLOW && passengerPreferences.getMusic() == PermissionType.ALLOW) {
                if(driverPreferences.getSmoking() == PermissionType.ALLOW && passengerPreferences.getSmoking() == PermissionType.ALLOW) {
                    return true;
                }
            }
        }
        return false;
    }
}
